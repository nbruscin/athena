# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

#####
# CI Reference Files Map
#####

# The top-level directory for the files is /eos/atlas/atlascerngroupdisk/data-art/grid-input/WorkflowReferences/
# Then the subfolders follow the format branch/test/version, i.e. for s3760 in master the reference files are under
# /eos/atlas/atlascerngroupdisk/data-art/grid-input/WorkflowReferences/main/s3760/v1 for v1 version

# Format is "test" : "version"
references_map = {
    # Simulation
    "s3761": "v14",
    "s4005": "v8",
    "s4006": "v15",
    "s4007": "v14",
    "s4008": "v1",
    "a913": "v10",
    # Digi
    "d1920": "v4",
    # Overlay
    "d1726": "v11",
    "d1759": "v17",
    "d1912": "v6",
    # Reco
    "q442": "v59",
    "q449": "v93",
    "q452": "v19",
    "q454": "v28",
    # Derivations
    "data_PHYS_Run2": "v29",
    "data_PHYSLITE_Run2": "v15",
    "data_PHYS_Run3": "v27",
    "data_PHYSLITE_Run3": "v14",
    "mc_PHYS_Run2": "v36",
    "mc_PHYSLITE_Run2": "v15",
    "mc_PHYS_Run3": "v36",
    "mc_PHYSLITE_Run3": "v16",
    "af3_PHYS_Run3": "v17",
    "af3_PHYSLITE_Run3": "v17",
}
